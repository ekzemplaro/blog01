import Head from 'next/head';
import Link from 'next/link';
import Layout, { siteTitle } from '../components/layout';
import utilStyles from '../styles/utils.module.css';

export default function Home() {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>栃木県下野市に住んでいます。東は、茨城県筑西市、西は栃木市、南は古河市、  北はさくら市が、行動範囲です。このあたりはサイクリングロードが充実しているので、自転車で行けます。</p>
        <p>
<Link href="/posts/first-post">
    <a>Read this page!</a>
  </Link>
        </p>
<p>May/26/2022 PM 17:38</p>
      </section>
    </Layout>
  );
}
